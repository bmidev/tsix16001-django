"""
WSGI config for scow project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

import sys
from django.core.wsgi import get_wsgi_application


sys.path.append('/home/gauravdave01/Development/my_venv')
sys.path.append('/home/gauravdave01/Development/my_venv/scow')
sys.path.append('/home/gauravdave01/Development/my_venv/scow/scow')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "scow.settings")

application = get_wsgi_application()
