from django.forms import ModelForm
from home.models import Entry


class EntryForm(ModelForm):
    class Meta:
        model = Entry
        fields = ['type', 'name', 'email', 'address_1', 'address_2', 'state', 'city', 'zip']
        exclude = ('user', 'product')
