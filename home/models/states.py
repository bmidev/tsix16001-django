from .platform_base_model import PlatformBaseModel
from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class State(PlatformBaseModel):
    name = models.CharField(max_length=80, blank=False, null=False, db_index=True)
    code = models.CharField(max_length=2, blank=False, null=False, db_index=True)

    def __str__(self):
        return self.name