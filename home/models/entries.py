from django.core.validators import RegexValidator
from django.db import models
from home.models import PlatformBaseModel, User, Product, State
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Entry(PlatformBaseModel):
    TYPES = (
        ('own', 'Own'),
        ('gift', 'Gift')
    )

    zip_validator = RegexValidator(
        regex=r"^[0-9]{5}$",
        message="Zip should only contain numbers and 5 digits long"
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    type = models.CharField(max_length=4, choices=TYPES, blank=False)
    name = models.CharField(max_length=250, blank=False)
    email = models.EmailField(unique=True, blank=False, db_index=True)
    address_1 = models.CharField(max_length=500, blank=False)
    address_2 = models.CharField(max_length=500, blank=True, null=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    city = models.CharField(max_length=150, blank=False)
    zip = models.CharField(max_length=5, validators=[zip_validator], blank=False)

    def __str__(self):
        return self.user.name