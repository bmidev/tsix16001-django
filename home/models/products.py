from home.models import PlatformBaseModel
from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Product(PlatformBaseModel):
    quantity = models.IntegerField()
    available_at = models.CharField(max_length=20)

    def __str__(self):
        return "Skinny Cow for Him"
