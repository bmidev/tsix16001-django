from home.models import PlatformBaseModel
from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class User(PlatformBaseModel):
    name = models.CharField(max_length=250, blank=False)
    email = models.EmailField(unique=True, blank=False, db_index=True)

    def __str__(self):
        return self.name
