from django.conf.urls import url
from . import views

app_name = "home"
urlpatterns = [
    url(r"^$", views.IndexView.as_view(), name="index"),
    url(r"^out-of-stock/", views.OutOfStockView.as_view(), name="oos"),
    url(r"^winner/", views.Winner.as_view(), name="winner"),
    url(r"^get-it/", views.get_it, name="get-it"),
    url(r"^get-left-count/", views.get_left_count, name="counter"),
    url(r"^promo-end/", views.PromoEndView.as_view(), name="po")
]