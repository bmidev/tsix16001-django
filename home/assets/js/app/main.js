var APP = APP || {};
var app = angular.module('siteApp', ['userController'], [
    '$interpolateProvider', function($interpolateProvider){
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }
]);
