var APP = APP || {};

APP.headerHomeOffset = 150;
APP.header = $('header');
APP.width = $(window).width();
APP.height = $(window).height();

APP.updateHeaderOffset = function () {
    APP.headerOffset = APP.header.innerHeight();
    if (APP.headerOffset > 100 && !APP.header.hasClass('sticky')) {
        APP.headerOffset -= 50;
    }
};

// Ready!!
$(document).ready(function () {
    APP.updateHeaderOffset();
	
	var htHead = $('header').innerHeight();
	$('.banner').css('margin-top', htHead);
	
    $(document).on('click', 'nav.main-links li a, .goto', function (event) {
        var $this = $(this);
        $('nav.main-links li').removeClass('active');
        $this.parent().addClass('active');

        var id = $this.attr("href");
        var offset = id === '#home' ? APP.headerHomeOffset : APP.headerOffset;
        var target = $(id).offset().top - offset;
        $('html, body').animate({
            scrollTop: target
        }, 1000);
        event.preventDefault();
    });

    // Mobile Navigation
    $('.mobile-toggle').click(function () {
        if ($('header').hasClass('open-nav')) {
            $('header').removeClass('open-nav');
        } else {
            $('header').addClass('open-nav');
        }
    });
	$('nav.main-links li a').click(function () {
        if ($('header').hasClass('open-nav')) {
            $('header').removeClass('open-nav');
        }
    });
    
    // Mobile Navigation End
    $('.video-area .video a').on('click', function (e) {
        e.preventDefault();
        var youtubeId = $(this).data('yt');
        var ht = $(this).find('> img').height();
        $(this).hide();
        $('.video-area .video').html('<iframe width="100%" height="' + ht + '" src="https://www.youtube.com/embed/' + youtubeId + '?autoplay=1&amp;rel=0&amp;showinfo=0&amp;wmode=transparent" frameborder="0" allowfullscreen ></iframe>');
    });

	$('.select-option input[type="radio"]').on('change', function() {
	   var optionVal = $('input[name="type"]:checked').val();
	   $('.select-form').hide();
	   $('.select-form.' + optionVal).show();
	});
	
	var getData = $('.counter span').first().html();
	if(getData > 0){
		var interval = 1000 * 60 * 5; // 5 Min Interval
		var get_left_timer = setInterval(get_left_count, interval);
	}
	
	function get_left_count() {
		var quantity = 0;
		$.ajax({
				type: 'POST',
				url: APP.baseUrl + '/get-left-count/',
				beforeSend: function(request) {
                    request.setRequestHeader("X-CSRFToken", $("[name='csrfmiddlewaretoken']").val());
                },
				async: false,
				success: function(result){
					if(result.status){
						quantity = result.data.toString();
						var qtyLen = quantity.length;
						var qtyCounter = '';
						for(var i=0; i<qtyLen; i++){
							qtyCounter += '<span>'+quantity[i]+'</span>'
						}
						$(".counter").html(qtyCounter);
					} else {
						$(".counter").html('<span>0</span>');
						clearInterval(get_left_timer);
						window.location.reload();
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log('Oops! Something went wrong. Please try again.');
					return false;
				}
		});
	}
});


$(window).scroll(function () {
    var windscroll = $(window).scrollTop();

    if (windscroll > 99) {
        $('header').addClass('sticky').addClass('fade-down');

    } else {
        $('header').removeClass('fade-down').removeClass('sticky');
    }
    APP.updateHeaderOffset();
});
