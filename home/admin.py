from django.contrib import admin

from home.forms.users import UserForm
from home.models import User, Entry, Product, State


class UserAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'created_at')


class EntryAdmin(admin.ModelAdmin):
    list_display = ('user', 'type', 'name', 'email', 'address_1', 'address_2', 'state', 'city', 'zip', 'created_at')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('quantity', 'available_at')


class StateAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')


admin.site.register(User, UserAdmin)
admin.site.register(Entry, EntryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(State, StateAdmin)
