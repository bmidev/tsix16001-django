/* Loading Libraries */
var gulp  = require('gulp'),
sass = require('gulp-sass'),
rename = require('gulp-rename'),
concat = require('gulp-concat'),
uglify = require('gulp-uglify'),
imagemin = require('gulp-imagemin'),
cache = require('gulp-cache'),
notify = require('gulp-notify'),
del = require('del'),

/* [Style] Source and Destination */
styleSrc = 'assets/sass/site.scss',
styleDest = 'static/home/css',
style_file = 'style.css',
style_file_min = 'style.min.css',

/* [JS] Source and Destination */
vendorScriptSrc = [
    'assets/js/vendor/jquery.min.js',
    'assets/js/vendor/angular.min.js'
],
vendor_file = 'vendor.js',
vendor_file_min = 'vendor.min.js',

appScriptSrc = [
    'assets/js/app/main.js',
    'assets/js/app/controllers/user-controller.js'
],
app_file = 'app.js',
app_file_min = 'app.min.js',

siteScriptSrc = 'assets/js/site.js',
scriptDest = 'static/home/js',
site_file = 'site.js',
site_file_min = 'site.min.js',

/* [IMG] Image Processing */
imageSrc = 'assets/images/*',
imageDest = 'static/home/images',

/* [Flags] Gulp Settings */
duoStyle = true,
duoScript = true,
styleRef = ['assets/sass/*.scss'],
scriptRef = ['assets/js/**/*'],
imgRef = ['assets/images/*'],
gulpTask = ['styles', 'scripts', 'images'];

/* [START] Styles Processing */
gulp.task('styles', function() {
    if(duoStyle) {
        gulp.src(styleSrc)
            .pipe(sass().on('error', sass.logError))
            .pipe(concat(style_file))
            .pipe(gulp.dest(styleDest))
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(concat(style_file_min))
            .pipe(gulp.dest(styleDest))
            .pipe(notify({ message: 'SCSS Compiled!' }));
    } else {
        gulp.src(styleSrc)
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(concat(style_file_min))
            .pipe(gulp.dest(styleDest))
            .pipe(notify({ message: 'SCSS Compiled!' }));
    }
});
/* [END] Styles Processing */

/* [START] Scripts Processing */
gulp.task('scripts', function() {
    if(duoScript) {
        gulp.src(vendorScriptSrc)
            .pipe(concat(vendor_file))
            .pipe(gulp.dest(scriptDest))
            .pipe(concat(vendor_file_min))
            .pipe(uglify())
            .pipe(gulp.dest(scriptDest))
            .pipe(notify({ message: '[Vendor] Scripts Compiled!' }));

        gulp.src(appScriptSrc)
            .pipe(concat(app_file))
            .pipe(gulp.dest(scriptDest))
            .pipe(concat(app_file_min))
            .pipe(uglify())
            .pipe(gulp.dest(scriptDest))
            .pipe(notify({ message: '[App] Scripts Compiled!' }));

        gulp.src(siteScriptSrc)
            .pipe(concat(site_file))
            .pipe(gulp.dest(scriptDest))
            .pipe(concat(site_file_min))
            .pipe(uglify())
            .pipe(gulp.dest(scriptDest))
            .pipe(notify({ message: '[Site] Scripts Compiled!' }));
    } else {
        gulp.src(vendorScriptSrc)
            .pipe(concat(vendor_file_min))
            .pipe(uglify())
            .pipe(gulp.dest(scriptDest))
            .pipe(notify({ message: '[Vendor] Scripts Compiled!' }));

        gulp.src(appScriptSrc)
            .pipe(concat(app_file_min))
            .pipe(uglify())
            .pipe(gulp.dest(scriptDest))
            .pipe(notify({ message: '[App] Scripts Compiled!' }));

        gulp.src(siteScriptSrc)
            .pipe(concat(site_file_min))
            .pipe(uglify())
            .pipe(gulp.dest(scriptDest))
            .pipe(notify({ message: '[Site] Scripts Compiled!' }));
    }
});
/* [END] Scripts Processing */

/* [START] Image Processing */
gulp.task('images', function() {
    gulp.src(imageSrc)
        .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
        .pipe(gulp.dest(imageDest))
        .pipe(notify({ message: 'Images Processed!' }));
});
/* [END] Image Processing */

/* [START] Clean Up! */
gulp.task('clean', function() {
    del([styleDest, scriptDest, imageDest]);
});
/* [END] Clean Up! */

/* [START] Compile & Watch */
gulp.task('default', ['clean'], function(){
    gulp.start(gulpTask)
    gulp.watch(styleRef, ['styles']);
    gulp.watch(scriptRef, ['scripts']);
    gulp.watch(imgRef, ['images']);
});
/* [END] Compile & Watch */