# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-25 09:57
from __future__ import unicode_literals

from django.db import migrations


def product_up(apps, schema_editor):
    Product = apps.get_model("home", "Product")
    db_alias = schema_editor.connection.alias

    Product.objects.using(db_alias).bulk_create([
        Product(quantity=100, available_at="2017-06-25"),
        Product(quantity=50, available_at="2017-06-30"),
        Product(quantity=50, available_at="2017-07-15"),
        Product(quantity=50, available_at="2017-07-25")
    ])


def product_down(apps, schema_editor):
    Product = apps.get_model("home", "Product")
    db_alias = schema_editor.connection.alias

    Product.objects.using(db_alias).all().delete()


def state_up(apps, schema_editor):
    State = apps.get_model("home", "State")
    db_alias = schema_editor.connection.alias

    State.objects.using(db_alias).bulk_create([
        State(name="Alabama", code="AL"),
        State(name="Arkansas", code="AR"),
        State(name="Arizona", code="AZ"),
        State(name="California", code="CA"),
        State(name="Colorado", code="CO"),
        State(name="Connecticut", code="CT"),
        State(name="District of Columbia", code="DC"),
        State(name="Delaware", code="DE"),
        State(name="Florida", code="FL"),
        State(name="Georgia", code="GA"),
        State(name="Iowa", code="IA"),
        State(name="Idaho", code="ID"),
        State(name="Illinois", code="IL"),
        State(name="Indiana", code="IN"),
        State(name="Kansas", code="KS"),
        State(name="Kentucky", code="KY"),
        State(name="Louisiana", code="LA"),
        State(name="Massachusetts", code="MA"),
        State(name="Maryland", code="MD"),
        State(name="Maine", code="ME"),
        State(name="Michigan", code="MI"),
        State(name="Minnesota", code="MN"),
        State(name="Missouri", code="MO"),
        State(name="Mississippi", code="MS"),
        State(name="Montana", code="MT"),
        State(name="North Carolina", code="NC"),
        State(name="North Dakota", code="ND"),
        State(name="Nebraska", code="NE"),
        State(name="New Hampshire", code="NH"),
        State(name="New Jersey", code="NJ"),
        State(name="New Mexico", code="NM"),
        State(name="Nevada", code="NV"),
        State(name="New York", code="NY"),
        State(name="Ohio", code="OH"),
        State(name="Oklahoma", code="OK"),
        State(name="Oregon", code="OR"),
        State(name="Pennsylvania", code="PA"),
        State(name="Rhode Island", code="RI"),
        State(name="South Carolina", code="SC"),
        State(name="South Dakota", code="SD"),
        State(name="Tennessee", code="TN"),
        State(name="Texas", code="TX"),
        State(name="Utah", code="UT"),
        State(name="Virginia", code="VA"),
        State(name="Vermont", code="VT"),
        State(name="Washington", code="WA"),
        State(name="Wisconsin", code="WI"),
        State(name="West Virginia", code="WV"),
        State(name="Wyoming", code="WY")
    ])


def state_down(apps, schema_editor):
    State = apps.get_model("home", "State")
    db_alias = schema_editor.connection.alias

    State.objects.using(db_alias).all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(product_up, product_down),
        migrations.RunPython(state_up, state_down)
    ]
