import datetime

from django.http import HttpResponse, JsonResponse
from django.views import generic

from home.forms.entries import EntryForm
from home.forms.users import UserForm
from home.models import State, Product, Entry


# Get Active Product Count
def get_today_product_count():
    current_date = datetime.datetime.today().strftime('%Y-%m-%d')
    pO = Product.objects.filter(available_at=current_date).values('quantity')

    if len(pO) == 0:
        return 0
    else:
        return pO[0]['quantity']


# Get Product ID (by Today's Date)
def get_product_id():
    current_date = datetime.datetime.today().strftime('%Y-%m-%d')
    pO = Product.objects.filter(available_at=current_date).values('id')

    if len(pO) == 0:
        return 0
    else:
        return pO[0]['id']


# Get Consumption Data Count
def get_today_entry_count():
    product_id = get_product_id()
    p_count = Entry.objects.filter(product_id=product_id).count()

    return p_count


# Get Quantity Left
def get_left_logic():
    qtyLeft = get_today_product_count() - get_today_entry_count()

    return qtyLeft


# Displays Index Page
class IndexView(generic.ListView):
    if get_left_logic() == 0:
        template_name = "home/pages/out_of_stock.html"
        context_object_name = ''

        def get_queryset(self):
            pass
    else:
        template_name = "home/pages/index.html"

        context_object_name = 'state_data'

        def get_queryset(self):
            return State.objects.all()

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['qty_left'] = get_left_logic()
        return context


# Displays Winner Page
class Winner(generic.ListView):
    template_name = "home/pages/winner.html"

    def get_queryset(self):
        pass

    def get_context_data(self, **kwargs):
        context = super(Winner, self).get_context_data(**kwargs)
        context['qty_left'] = get_left_logic()
        return context


# Displays Out of Stock Page
class OutOfStockView(generic.ListView):
    template_name = "home/pages/out_of_stock.html"

    def get_queryset(self):
        pass


class PromoEndView(generic.ListView):
    template_name = "home/pages/promo_end.html"

    def get_queryset(self):
        pass

    def get_context_data(self, **kwargs):
        context = super(PromoEndView, self).get_context_data(**kwargs)
        context['qty_left'] = 0
        return context


# [API] Get Product Left Count
def get_left_count(request):
    qtyLeft = get_left_logic()
    return JsonResponse({'status': True, 'message': 'Quantity Left', 'data': qtyLeft})


# [Store] Create User and Entry
def get_it(request):
    user_object = UserForm(request.POST)
    entry_object = EntryForm(request.POST)

    if get_left_logic() == 0:
        return JsonResponse({'status': False, 'message': 'Out of Stock', 'data': 'Entry Expired'}, status=422)
    else:
        product_id = get_product_id()

    if user_object.is_valid() and entry_object.is_valid():
        uO = user_object.save()
        eO = entry_object.save(commit=False)
        eO.user_id = uO.pk
        eO.product_id = product_id

        if request.POST['type'] == 'gift':
            eO.name = request.POST['gift_holder_name']
            eO.email = request.POST['gift_holder_email']

        eO.save()

        return JsonResponse({'status': True, 'message': 'Success', 'data': {}})
    else:
        errors = {}
        errors.update(entry_object.errors)
        errors.update(user_object.errors)
        return JsonResponse({'status': False, 'message': 'Validation Error', 'data': errors}, status=422)
