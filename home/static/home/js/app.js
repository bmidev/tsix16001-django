var APP = APP || {};
var app = angular.module('siteApp', ['userController'], [
    '$interpolateProvider', function($interpolateProvider){
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }
]);

angular.module('userController', [])
.controller('register', function($scope, $http) {
    $scope.user = {};
    $scope.errors = {};

    $scope.submit = function() {
        $scope.errors = {};
        $http({
            method: 'POST',
            url: APP.baseUrl + 'get-it/',
            data: $.param($scope.user),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-CSRFToken': $("[name='csrfmiddlewaretoken']").val()
            }
        }).success(function(response) {
			if(response.status){
				window.location.href = APP.baseUrl + 'winner#forms';
			} else {
				window.location.href = APP.baseUrl + '#forms';
			}
        }).error(function(response, status) {
            if (status === 422) {
                $scope.errors = response.data;
            } else {
                $scope.errors['error'] = ['An error occurred, please try again later'];
            }
        });
    };
});